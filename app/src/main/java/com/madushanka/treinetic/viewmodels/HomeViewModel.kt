package com.madushanka.treinetic.viewmodels

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.madushanka.treinetic.data.Repository
import com.madushanka.treinetic.models.Product
import com.madushanka.treinetic.util.*
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeViewModel @ViewModelInject constructor(
    private val repository: Repository,
    application: Application
) : AndroidViewModel(application) {
    var productListLiveData =  MutableLiveData<Resource<List<Product>>>()

    @RequiresApi(Build.VERSION_CODES.M)
    fun getAllProducts() = viewModelScope.launch {
        getAllProductSafeCall()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun getAllProductSafeCall() {

            productListLiveData.setLoading()
            val response = repository.remote.getRecipes()
            response.enqueue(object : Callback<List<Product>> {
                override fun onResponse(call: Call<List<Product>>, response: Response<List<Product>>) {
                    val data: List<Product>  = (response.body() as ArrayList<Product>?)!!
                    productListLiveData.setSuccess(data,null)

                }

                override fun onFailure(call: Call<List<Product>>, t: Throwable) {
                    productListLiveData.setError(t.message.toString())
                }

            })



    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun hasInternetConnection(): Boolean {
        val connectivityManager = getApplication<Application>().getSystemService(
            Context.CONNECTIVITY_SERVICE
        ) as ConnectivityManager
        val activeNetwork = connectivityManager.activeNetwork ?: return false
        val capabilities = connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
        return when {
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }
    }
}