package com.madushanka.treinetic.viewmodels

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.madushanka.treinetic.data.Repository
import com.madushanka.treinetic.models.Product
import com.madushanka.treinetic.util.Resource
import com.madushanka.treinetic.util.setError
import com.madushanka.treinetic.util.setLoading
import com.madushanka.treinetic.util.setSuccess
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FeatureViewModel @ViewModelInject constructor(
    private val repository: Repository,
    application: Application
) : AndroidViewModel(application){

    var featureProductLiveData =  MutableLiveData<Resource<Product>>()


    fun getFeatureProduct() = viewModelScope.launch {
        getFeatureProductSafeCall()
    }


    private fun getFeatureProductSafeCall() {

            featureProductLiveData.setLoading()
            val response = repository.remote.getFeatured()
            response.enqueue(object : Callback<Product> {
                override fun onResponse(call: Call<Product>, response: Response<Product>) {
                    val data: Product  = (response.body() as Product)
                    featureProductLiveData.setSuccess(data,null)

                }
                override fun onFailure(call: Call<Product>, t: Throwable) {
                    featureProductLiveData.setError(t.message.toString())
                }

            })



    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun hasInternetConnection(): Boolean {
        val connectivityManager = getApplication<Application>().getSystemService(
            Context.CONNECTIVITY_SERVICE
        ) as ConnectivityManager
        val activeNetwork = connectivityManager.activeNetwork ?: return false
        val capabilities = connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
        return when {
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }
    }
}