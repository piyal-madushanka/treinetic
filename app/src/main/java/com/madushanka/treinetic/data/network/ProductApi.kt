package com.madushanka.treinetic.data.network

import com.madushanka.treinetic.models.Product
import retrofit2.Call
import retrofit2.http.GET

interface ProductApi {
    @GET("/products")
     fun getAllProducts(): Call<List<Product>>

     @GET("/featured")
     fun getFeatured():Call<Product>
}
