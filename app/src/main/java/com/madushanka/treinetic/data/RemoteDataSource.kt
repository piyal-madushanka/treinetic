package com.madushanka.treinetic.data

import com.madushanka.treinetic.data.network.ProductApi
import com.madushanka.treinetic.models.Product
import retrofit2.Call
import javax.inject.Inject

class RemoteDataSource @Inject constructor(
    private val productApi: ProductApi
) {
 fun getRecipes(): Call<List<Product>> {
        return productApi.getAllProducts()
    }
    fun getFeatured():Call<Product>{
        return productApi.getFeatured()
    }
}