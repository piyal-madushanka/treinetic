package com.madushanka.treinetic.di

import com.madushanka.treinetic.BuildConfig
import com.madushanka.treinetic.util.Constants.Companion.BASE_URL
import com.madushanka.treinetic.data.network.ProductApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun provideSupportInterCeptor():SupportInterceptor{
        return SupportInterceptor("")
    }

    @Singleton
    @Provides
    fun provideHttpClient(
        supportInterceptor: SupportInterceptor
    ) : OkHttpClient {
        val httpLoggingInterceptor = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger.DEFAULT)
        val clientBuilder = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) {
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            clientBuilder.addInterceptor(httpLoggingInterceptor)
        }

//    TODO handle 401 and interact user to re-authenticate using Authenticator, see @SupportInterceptor fun authenticate()
        clientBuilder.authenticator(supportInterceptor)
        clientBuilder.addInterceptor(supportInterceptor)
        clientBuilder.connectTimeout(60,TimeUnit.SECONDS)
        clientBuilder.writeTimeout(120,TimeUnit.SECONDS)
        clientBuilder.readTimeout(120,TimeUnit.SECONDS)

        return clientBuilder.build()
    }

    @Singleton
    @Provides
    fun provideConverterFactory(): GsonConverterFactory {
        return GsonConverterFactory.create()
    }

    @Singleton
    @Provides
    fun provideRetrofitInstance(
        okHttpClient: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(gsonConverterFactory)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    @Singleton
    @Provides
    fun provideApiService(retrofit: Retrofit): ProductApi {
        return retrofit.create(ProductApi::class.java)
    }

}