package com.madushanka.treinetic.ui.activity

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.madushanka.treinetic.R
import com.madushanka.treinetic.models.Product
import com.madushanka.treinetic.util.Resource
import com.madushanka.treinetic.util.ResourceState
import com.madushanka.treinetic.util.showToast
import com.madushanka.treinetic.util.withNetwork
import com.madushanka.treinetic.viewmodels.FeatureViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_feature_product_activty.*

@AndroidEntryPoint
class FeatureProductActivity : AppCompatActivity() {

    private lateinit var featureViewModel: FeatureViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feature_product_activty)
        statusBarColorChange()
        featureViewModel = ViewModelProvider(this).get(FeatureViewModel::class.java)
        withNetwork({
            featureViewModel.getFeatureProduct()
        }) {
            "Network connection not available.".showToast(this)
        }

        featureViewModel.featureProductLiveData.observe(
            this,
            Observer { updateProfile(it) })
        imb_back.setOnClickListener{
            onBackPressed()
        }
    }

    private fun updateProfile(resource: Resource<Product>) {
        resource.let {
            when (it.state) {
                ResourceState.LOADING -> {
                }
                ResourceState.SUCCESS -> {
                    it.data?.let { it1 -> setData(it1) }
                }
                ResourceState.ERROR -> {
                    this.let { it1 -> it.message.toString().showToast(it1) }
                }
            }
        }
    }
    @SuppressLint("SetTextI18n")
    private fun setData(product: Product){
        txt_product_name.text  = product.title
        txt_product_price.text = product.price
        txt_product_description.text = product.description
        txt_rate_value.text = product.rating.toString()+" (28 reviews)"
        ratingBar.rating = product.rating!!
    }

    private fun statusBarColorChange() {
        val window: Window = this.window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.lowBlue)
    }
}