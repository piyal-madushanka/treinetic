package com.madushanka.treinetic.ui.fragments.home

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.madushanka.treinetic.R
import com.madushanka.treinetic.models.Product
import com.madushanka.treinetic.ui.activity.FeatureProductActivity
import com.madushanka.treinetic.ui.fragments.home.adapters.ItemListener
import com.madushanka.treinetic.ui.fragments.home.adapters.ProductAdapter
import com.madushanka.treinetic.util.Resource
import com.madushanka.treinetic.util.ResourceState
import com.madushanka.treinetic.util.showToast
import com.madushanka.treinetic.util.withNetwork
import com.madushanka.treinetic.viewmodels.HomeViewModel
import com.todkars.shimmer.ShimmerRecyclerView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*


@AndroidEntryPoint
class HomeFragment : Fragment() {

    lateinit var productAdapter: ProductAdapter
    private lateinit var homeViewModel: HomeViewModel
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        productAdapter = ProductAdapter(object : ItemListener {
            override fun onItemClick() {
                val intent = Intent(context, FeatureProductActivity::class.java)
                startActivity(intent)
            }

        })
        val storyLayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        view.recycler_view.layoutManager = storyLayoutManager
        view.recycler_view.adapter = productAdapter
        showShimmerEffect(view.recycler_view)
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        context?.withNetwork({
            homeViewModel.getAllProducts()
        }) {
            "Network connection not available.".showToast(requireContext())
        }

        homeViewModel.productListLiveData.observe(
            viewLifecycleOwner,
            Observer { updateProfile(it) })
        return view
    }

    private fun showShimmerEffect(recyclerView: ShimmerRecyclerView) {
        recyclerView.showShimmer()
    }

    private fun hideShimmerEffect(recyclerView: ShimmerRecyclerView) {
        recyclerView.hideShimmer()
    }

    private fun updateProfile(resource: Resource<List<Product>>) {
        resource.let {
            when (it.state) {
                ResourceState.LOADING -> {
                    showShimmerEffect(recycler_view)
                }
                ResourceState.SUCCESS -> {
                    hideShimmerEffect(recycler_view)
                    it.data?.let { it1 -> productAdapter.setDataToAdapter(it1) }
                    Log.e("data", it.data?.size.toString())
                }
                ResourceState.ERROR -> {
                    showShimmerEffect(recycler_view)

                    context?.let { it1 -> it.message.toString().showToast(it1) }
                }
            }
        }
    }
}