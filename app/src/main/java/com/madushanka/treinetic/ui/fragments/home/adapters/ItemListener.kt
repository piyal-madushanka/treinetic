package com.madushanka.treinetic.ui.fragments.home.adapters

interface ItemListener {
    fun onItemClick()
}