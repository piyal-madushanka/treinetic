package com.madushanka.treinetic.ui.fragments.home.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.madushanka.treinetic.R
import com.madushanka.treinetic.models.Product
import kotlinx.android.synthetic.main.item_product_card.view.*

class ProductAdapter(
    var listener: ItemListener
) : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {
    var allProductList: List<Product> = listOf()

    fun setDataToAdapter(list: List<Product>) {
        allProductList = list
    }

    inner class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val productImage: ImageView = itemView.iv_item
        val productName: TextView = itemView.txt_name
        val productPrice: TextView = itemView.txt_price
        val plusBtn: ImageView = itemView.iv_button
    }

    private fun loadImageFromUrl(imageView: ImageView, imageUrl: String) {
        imageView.load(imageUrl) {
            crossfade(600)
            error(R.drawable.ic_error_placeholder)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_product_card, parent, false)
        return ProductViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        var product = allProductList[position]
        product.images?.let { loadImageFromUrl(holder.productImage, it) }
        holder.productName.text = product.title
        holder.productPrice.text = product.price
        holder.itemView.setOnClickListener {
            listener.onItemClick()
        }
    }

    override fun getItemCount(): Int {
        return allProductList.size
    }
}