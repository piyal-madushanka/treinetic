package com.madushanka.treinetic.models

import com.google.gson.annotations.SerializedName

data class Product(
    @SerializedName("id")
    var id: String? = null,
    @SerializedName("title")
    var title: String? = null,
    @SerializedName("price")
    var price: String? = null,
    @SerializedName("rating")
    var rating: Float? = null,
    @SerializedName("description")
    var description: String? = null,
    @SerializedName("images")
    var images: String? = null
)
